OBJECTS = counts.o tests.o

# OS identification from: https://stackoverflow.com/questions/714100/os-detecting-makefile
 OS := $(shell uname -s)

 ifeq ($(OS), Darwin)
   CUNIT_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/
     CUNIT_DIRECTORY = cunit
     endif
     ifeq ($(OS), Linux)
       CUNIT_PATH_PREFIX = /util/CUnit/
         CUNIT_DIRECTORY = CUnit/
         endif

         CC = gcc
         FLAGS = -g -c -Wall -std=c11
         
         SRC = week.c
         TST = tests.c
         CFLAGS = -L $(CUNIT_PATH_PREFIX)lib -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY)
         CLIB = -lcunit

.PHONY: clean
clean: 
	rm -rf *~ *.o *.dSYM

week.o: week.c
	$(CC) $(FLAGS) $(SRC)

tests.o: tests.c
	$(CC) -c $(FLAGS) -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY) $(TST) 

week: week.o
	$(CC) $(FLAGS) $(SRC)

tests: tests.o week.o
	gcc -g -Wall -L $(CUNIT_PATH_PREFIX)lib -I $(CUNIT_PATH_PREFIX)includes/$(CUNIT_DIRECTORY) -lm -o tests week.o  tests.o -lcunit
