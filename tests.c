#include <stdio.h>
#include <string.h>
#include "CUnit.h"
#include "Basic.h"
#include "week.c"

// Tests
int numArgs01(void){ // Is the
  int expected = 0;
  int num = 3; 
  char* date = "YYYY-MM-DD";
  int actual = isInputCorrect(num, date);
  CU_ASSERT_EQUAL( expected, actual  );
}

int numArgs02(void){
  int expected = 1;
  int num = 2; 
  char* date = "YYYY-MM-DD";
  int actual = isInputCorrect(num, date);
  CU_ASSERT_EQUAL( expected, actual  );
}

int dateLength01(void){ // Is the length correct? 
  int expected = 0;
  int num = 2;
  char* date = "YYYY-MM-DD0";
  int actual = isInputCorrect(num, date);
  CU_ASSERT_EQUAL( expected, actual  );
}

int dateLength02(void){
  int expected = 1;
  int num = 2;
  char* date = "YYYY-MM-DD";
  int actual = isInputCorrect(num, date);
  CU_ASSERT_EQUAL( expected, actual  );
}

/* The main() function for setting up and running the tests.
 *  * Returns a CUE_SUCCESS on successful running, another
 *   * CUnit error code on failure.
 *    */
int main()
{
   CU_pSuite Suite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   Suite = CU_add_suite("Suite_of_tests_with_valid_inputs", NULL, NULL);
   if (NULL == Suite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to Suite */

if (
          (NULL == CU_add_test(Suite, "numArgs01", numArgs01))
        ||(NULL == CU_add_test(Suite, "numArgs02", numArgs02))
        ||(NULL == CU_add_test(Suite, "dateLength01", dateLength01))
        ||(NULL == CU_add_test(Suite, "dateLength02", dateLength02))
    )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using automated interface, with output to screen' */
CU_set_output_filename("test");
   CU_automated_run_tests();

   CU_cleanup_registry();
   return CU_get_error();
}

